import os
import re
#  from datetime import datetime

class Sample():
    """Sample class that holds general information about sample.
    Instance should be created with address argument, which is path to main folder"""
    process_names = ('EVAPMAG', 'MAG', 'NS', 'COEV', 'EVAP', 'MMD', 'EBE')
    drum_lines = ('L', 'C', 'R')
    cut_lines = ('left', 'center', 'right')

    def __init__(self, address: str, keithleyfile: bool = False):
        self.keithleyfile = keithleyfile
        if os.path.isdir(address) and os.path.split(address)[1] == 'raw_data':
            self.address = os.path.split(address)[0]
        else:
            self.address = address
        self.path_head, self.path_tail = os.path.split(self.address)

        if re.search(r'__\d.xls', self.path_tail):
            raise NotImplementedError("Can't deal with split excel files")
        # self.fields = self.last_pathname.split('_')
        self._fields = self.get_fields_from_filename()
        self.cathode_name = self.get_cathode_name()
        self.pos = self.get_longitudinal_pos()
        self.drum_line = self.get_drum_line()
        self.battery_number = self.get_battery_number()
        self.cutline = self.get_cutline()
        self.annealed = self.is_annealed()
        self.normalized_fn = self.get_normalized_folder_name()

    def __str__(self):
        fields = {'cathode': self.cathode_name, 'sampleID': self.normalized_fn, 'battery_num': self.battery_number,
                  'position': self.pos, 'cutline': self.cutline, 'annealed': self.annealed, 'drumline': self.drum_line}
        return ", ".join(f"{key}: {value} " for key, value in fields.items())

    def __repr__(self):
        return self.normalized_fn

    def get_fields_from_filename(self):
        fields = self.path_tail.split('_')
        if self.path_tail.endswith('.xls'):  # remove unnecesary info from filename for china stand files
            fields = fields[:-2]
        return fields

    def get_cathode_name(self) -> str:
        fields_upper = [n.upper() for n in self._fields]
        sample_name = [i for i in fields_upper if any(j in i for j in self.process_names)]
        if sample_name:
            idx = fields_upper.index(sample_name[0])
        else:
            idx = 0
        cathode_name = fields_upper[idx]
        self._fields.pop(idx)
        process = [process_name for process_name in self.process_names if process_name in cathode_name]
        if process:
            if process[0] == "MAG" or process[0] == "EVAPMAG":
                process_date = cathode_name.split(process[0])[1]
                day = process_date[0:2]
                month = process_date[2:4]
                year = process_date[4:8]

                if len(year) == 2:
                    year = "20"+year
                elif not year:
                    # year = str(datetime.now().year)
                    year = ""
                cathode_name = process[0] + day + month + year
            self.exception = False
        else:
            self.exception = True
        return cathode_name

    def get_longitudinal_pos(self) -> str:
        # more reliable way in case there'll be more positions
        result = re.search(r'(H\d*-\d*|H\d*)+', "_".join(self._fields))
        if result:
            pos = result.group()
            self._fields.pop(self._fields.index(pos))
            if len(pos) == 2:  # case for H4, H5 etc
                pos = pos
            elif '-' not in pos:
                pos = pos[0:2] + '-' + pos[2:]
        else:
            pos = "N/A"
        return pos

    def get_drum_line(self) -> str:
        # fields = self.folder_name.split('_')
        line = [i.upper() for i in self._fields if any(j == i.upper() for j in self.drum_lines)]
        if line:
            drum_line = line[0]
            self._fields.pop(self._fields.index(drum_line))
        else:
            drum_line = "N/A"
        return drum_line

    def get_cutline(self) -> str:
        cutline = [i.lower() for i in self._fields if any(j in i.lower() for j in self.cut_lines)]
        if cutline:
            cutline = cutline[0]
            self._fields.pop(self._fields.index(cutline))
        else:
            cutline = "N/A"
        return cutline

    def get_battery_number(self) -> int:
        # patterns to find battery number: B{num} or BAT{num}
        result = re.search(r'(?i)(b\d+)|(bat\d+)|(new\d+)', "_".join(self._fields))
        if result:
            bat_num_ = result.group().upper()
            upper_fields = [i.upper() for i in self._fields]
            self._fields.pop(upper_fields.index(bat_num_))
        else:
            bat_num_ = str(0)
        try:
            battery_number = int(''.join(filter(str.isdigit, bat_num_)))
        except ValueError:
            battery_number = 0
        return battery_number

    def is_annealed(self):
        # fields = [x.lower() for x in self._fields]
        # if 'not' in fields and 'annealed' in fields or 'notannealed' in fields:
        #     annealed = False
        # elif 'annealed' in fields:
        #     self._fields.pop(self._fields.index('annealed'))
        #     annealed = True
        # else:
        #     annealed = "N/A"
        match = [i.lower() for i in self._fields if 'annealed' in i]
        if match:
            idx = self._fields.index(match[0])
            if match[0] == 'notannealed':
                self._fields.pop(idx)
                annealed = "not_annealed"
            elif match[0] == 'annealed' and self._fields[idx - 1] == 'not':
                self._fields.pop(idx)       # pop higher index first so order is not disturbed for lower indexes
                self._fields.pop(idx - 1)
                annealed = "not_annealed"
            elif match[0] == 'annealed':
                self._fields.pop(idx)
                annealed = "annealed"
            else:
                annealed = "N/A"  # leave for manual fix if description is faulty
        else:
            annealed = "N/A"
        return annealed

    def get_normalized_folder_name(self) -> str:
        battery_num = f'B{self.battery_number:02d}'
        all_fields = [self.cathode_name, self.pos, self.drum_line, self.cutline, self.annealed]
        all_fields.extend(self._fields)
        all_fields.append(battery_num)
        # full_name_ = (self.cathode_name, self.pos, self.drum_line, self.cutline, self.annealed, self._fields, battery_num)
        folder_name = '_'.join([f for f in all_fields if "N/A" not in f])
        return folder_name

        # this doesn't work with EBE samples
        # field = [x for x in self.fields if 'B' in x.upper() or 'BAT' in x.upper()]
        # if field:
        #     field = field[0]
        # else:
        #     field = self.fields[-1]
        # try:
        #     battery_number = int(''.join(filter(str.isdigit, field)))
        # except ValueError:
        #     battery_number = 0
        # return battery_number


        # fields = self.folder_name.split('_')
        # pos = [x for x in self.fields if x[0] == 'H']
        # if pos:
        #     pos = pos[0]
        #     if len(pos) == 3 and '-' not in pos:
        #         pos = pos[0:2] + '-' + pos[2:]
        # else:
        #     pos = "N/A"
        # return pos
