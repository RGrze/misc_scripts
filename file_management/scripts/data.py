import os
from datetime import datetime
from collections import Counter
import pandas as pd
from numpy import polyfit

from scripts.sample import Sample


def get_measurement_files(sample_path: str) -> list:
    """
    Get all files that are inside sample's raw_data folder
    :return: list that contain full path to files
    """
    if sample_path.endswith('raw_data'):
        data_path = sample_path
    elif os.path.isdir(os.path.join(sample_path, 'raw_data')):
        data_path = os.path.join(sample_path, 'raw_data')
    else:
        return []
    files = [os.path.join(data_path, f) for f in os.listdir(data_path)
             if os.path.isfile(os.path.join(data_path, f))]
    files.sort(key=lambda x: os.path.getmtime(x))
    return files


def update_batteries_database(battery_df_address: str, sample: Sample, summary: list):
    if not os.path.isfile(battery_df_address):
        with open(battery_df_address, 'w') as f:
            headers = ['Date of processing', 'Process', 'Long. position', 'Drum line', 'Annealed',
                       'Battery num', 'No of cycles', 'Capacity (mAh)', 'Deg. rate %', 'Last processing date',
                       'Init. chg. current (mA)', 'Finish chg. current (mA)', 'Init. dchg. current (mA)',
                       'Finish dchg. current (mA)', 'Capacity density', 'Overwrite flag']
            text = "\t".join(headers)
            f.write(f"{text}\n")

    battery_df = pd.read_csv(battery_df_address, sep='\t', keep_default_na=False)
    # battery_df.replace({pd.np.nan: None}, inplace=True)
    df_condition = (battery_df["Process"] == sample.normalized_fn) \
                   & (battery_df["Long. position"] == sample.pos) \
                   & (battery_df["Drum line"] == sample.drum_line) \
                   & (battery_df["Annealed"] == sample.annealed) \
                   & (battery_df["Battery num"] == sample.battery_number)

    # look for overwriting protection flag
    if not battery_df.loc[df_condition].empty:
        if battery_df.loc[df_condition, 'Overwrite flag'].values[0]:
            raise FlagOverwriteException(f"{sample.normalized_fn} - overwrite protection flag")

    def get_mode():
        start_current_chg = [summary[i]['Start current'] for i in range(len(summary)) if
                             summary[i]['Measurement type'] == 'charge'] or [0]
        stop_current_chg = [summary[i]['Stop current'] for i in range(len(summary)) if
                            summary[i]['Measurement type'] == 'charge'] or [0]
        start_current_dchg = [summary[i]['Start current'] for i in range(len(summary)) if
                              summary[i]['Measurement type'] == 'discharge'] or [0]
        stop_current_dchg = [summary[i]['Stop current'] for i in range(len(summary)) if
                             summary[i]['Measurement type'] == 'discharge'] or [0]

        start_current_chg = Counter(start_current_chg).most_common()[0][0]
        stop_current_chg = Counter(stop_current_chg).most_common()[0][0]
        start_current_dchg = Counter(start_current_dchg).most_common()[0][0]
        stop_current_dchg = Counter(stop_current_dchg).most_common()[0][0]
        return start_current_chg, stop_current_chg, start_current_dchg, stop_current_dchg

    start_current_chg, stop_current_chg, start_current_dchg, stop_current_dchg = get_mode()

    def get_degradation_rate():
        capacity = [summary[i]['Total capacity'] for i in range(len(summary)) if
                    summary[i]['Measurement type'] == 'discharge']

        if len(capacity) < 4:
            deg_rate = "N/A"
        else:
            slope, intercept = polyfit(range(len(capacity)), capacity, 1)
            deg_rate = round((slope / intercept) * 100, 4)
        return deg_rate

    deg_rate = get_degradation_rate()

    # get rest of info about sample from summary
    initial_process_date = summary[0]['Date']
    last_process_date = summary[-1]['Date']
    cycles = summary[-1]['Cycle']
    capacity = summary[-1]['Total capacity']
    capacity_density = 0

    # this dict contains every information about sample, will be placed inside database as row:
    to_update = {"Date of processing": initial_process_date,
                 "No of cycles": cycles, "Capacity (mAh)": round(capacity, 4), "Deg. rate %": deg_rate,
                 "Last processing date": last_process_date,
                 "Init. chg. current (mA)": round(start_current_chg, 4),
                 "Finish chg. current (mA)": round(stop_current_chg, 4),
                 "Init. dchg. current (mA)": round(start_current_dchg, 4),
                 "Finish dchg. current (mA)": round(stop_current_dchg, 4),
                 "Capacity density": capacity_density, 'Overwrite flag': 0}

    if battery_df.loc[df_condition].empty:
        row = pd.Series({"Process": sample.normalized_fn, "Long. position": sample.pos,
                         "Drum line": sample.drum_line, "Annealed": sample.annealed,
                         "Battery num": sample.battery_number, **to_update})
        battery_df = battery_df.append(row, ignore_index=True)
    else:
        update_keys, update_values = zip(*to_update.items())
        # update df
        battery_df.loc[df_condition, update_keys] = update_values
    battery_df.to_csv(battery_df_address, sep='\t', index=False, na_rep='N/A')
    return battery_df


def create_sample_summary(sample: Sample) -> list:
    def _get_df(file) -> pd.DataFrame:
        file_mode = file[-3:]

        if file_mode == 'csv':
            df = pd.read_csv(file, sep='\t')
            try:
                if "Current" not in df.columns:  # older files didn't have Current column
                    df["Current"] = df["Capacity"] / (df["Time"] / 3600)
            except:
                df["Current"] = 0
            finally:
                return df

        elif file_mode == 'xls':
            # TODO: profile code to improve speed for xls files?
            def convert_to_seconds(data: str) -> float:
                data = data.replace(':', '.').split('.')
                return int(data[0]) * 3600 + int(data[1]) * 60 + int(data[2]) + (int(data[3]) / 10E2)

            xls = pd.ExcelFile(file)
            data_sheets = [sheet for sheet in xls.sheet_names if 'Detail' in sheet]

            df_list = []
            for sheet in data_sheets:
                __df = pd.read_excel(xls, sheet)
                df_list.append(__df)

            df = pd.concat([df for df in df_list], ignore_index=True)

            _df = df.iloc[:, [1, 3, 5, 6, 7, 9, 10]]
            _df = _df.copy()  # get rid of SettingWithCopyWarning
            # _df.is_copy = False  #  This is going to be deprecated in new pandas

            # In case that excel file will be saved in chinese, replace all required fields to normalized names
            columns = _df.columns

            # print(_df.columns)
            _df.rename(columns={columns[0]: "Status",
                                columns[1]: "Cycle",
                                columns[2]: "Current",
                                columns[3]: "Voltage",
                                columns[4]: "Capacity",
                                columns[5]: "Time",
                                columns[6]: "Absolute Time"},
                       inplace=True)
            _df['Time'] = [convert_to_seconds(x) for x in _df['Time']]
            return _df

    def _get_cycle_summary_data(df: pd.DataFrame, source: str):
        """From single measurement we want to obtain data for summary:
        - Cycle ID
        - Date of measurement
        - Measurement type: charge / discharge
        - Charge / discharge mode: CC / CCCV
        - Total capacity
        - Total time elapsed
        - Start current
        - Stop current
        - CV / CC ratio

        In this function we obtain every information for summary but Cycle ID, which has to be calculated outside,
        in case there are log files from different sources, or multiple excel files (each excel file starts
        counting from 1 cycle).

        Function will return dict with data for one measurement if it's our .csv log, otherwise it will return
        list of dicts for every operation that's inside excel file
        """
        try:
            # Getting data has to be different for our csv files and chinese .xls files
            def convert_time(seconds):
                minutes = seconds // 60
                hours = minutes // 60
                return ("%02d:%02d:%02d" % (hours, minutes % 60, seconds % 60))

            if source.endswith('csv'):

                # split filename to fields:
                fn = os.path.split(source)[1].split('_')
                # Get date of measurement from file name:
                _fn_date = fn[0]  # our logs always starts from date

                try:
                    fn_date = datetime.strptime(_fn_date, "%Y.%m.%d--%H.%M.%S").__str__()
                except ValueError:
                    fn_date = datetime.strptime(_fn_date, "%Y.%m.%d-%H.%M.%S").__str__()

                # Get measurement from file name
                measurement_type = fn[-2]

                last_row = df.iloc[-1]
                if round(df.iloc[0]['Current'], 7) != round(last_row['Current'], 7):
                    measurement_mode = 'CCCV'
                else:
                    measurement_mode = 'CC'

                total_capacity = round(last_row['Capacity'], 7)
                total_time = convert_time(last_row['Time'])
                start_current = round(df.iloc[0]['Current'], 6)
                stop_current = round(last_row['Current'], 6)

                # Get last record for capacity from filtered dataframe, that contains only CC records
                cc_cap = df.query('Current == Current[0]')['Capacity'].iloc[-1]
                cv_cap = total_capacity - cc_cap

                if measurement_mode == 'CCCV':
                    cv_cc_ratio = round(cv_cap / cc_cap, 4)
                    if cv_cc_ratio < 0:
                        cv_cc_ratio = 0
                else:
                    cv_cc_ratio = 0

                data = {"Date": fn_date, "Measurement type": measurement_type, "Measurement mode": measurement_mode,
                        "Total capacity": total_capacity, "Total time": total_time, "Start current": start_current,
                        "Stop current": stop_current, "CV/CC": cv_cc_ratio
                        }
                return data
            elif source.endswith('xls'):
                """From get_df function we receive trimmed dataframe which contains only required information.
                All the function has to do is to split each operation, fetch data, update data dict and append list with it
                """

                def get_data(_df, measurement_type):
                    fn_date = _df["Absolute Time"].iloc[0].__str__()
                    measurement_mode = _df["Status"].iloc[0].split("_")[0]
                    total_capacity = round(_df["Capacity"].iloc[-1], 3)
                    total_time = convert_time(_df["Time"].iloc[-1])
                    start_current = _df["Current"].mode()[0]  # most frequent value
                    stop_current = round(_df["Current"].iloc[-1], 4)

                    cc_cap = _df.query(f"Current == {start_current}")["Capacity"].iloc[-1]
                    cv_cap = total_capacity - cc_cap
                    cv_cc_ratio = round(cv_cap / cc_cap, 4)

                    data = {"Date": fn_date, "Measurement type": measurement_type, "Measurement mode": measurement_mode,
                            "Total capacity": total_capacity, "Total time": total_time, "Start current": start_current,
                            "Stop current": stop_current, "CV/CC": cv_cc_ratio
                            }
                    return data

                data_list = []
                # use pandas query() function to split dataframe:
                cycle = 1
                last_cycle = df['Cycle'].iloc[-1]
                while cycle < last_cycle:
                    # get records for charge at specified cycle
                    _df = df.query(f"Status.str.contains('DChg|Rest') == False & Cycle == '{cycle}' ")
                    data = get_data(_df, "charge")
                    data_list.append(data)

                    # get records for discharge at specified cycle
                    _df = df.query(f"Status.str.contains('DChg') & Cycle == '{cycle}' ")
                    data = get_data(_df, "discharge")
                    data_list.append(data)

                    cycle += 1
                return data_list
        except:
            raise FetchDataError("Unable to fetch summary data for this file")

    files = get_measurement_files(sample.address)
    try:
        dfs = [_get_df(file) for file in files]
    except pd.errors.ParserError:
        dfs = []

    if not files or not dfs:
        return []
    # get summary for each individual operation for sample
    try:
        summary_ = [_get_cycle_summary_data(df, f) for df, f in zip(dfs, files)]
    except FetchDataError:
        return []

    summary = []
    for item in summary_:
        if isinstance(item, list):
            for j in item:
                summary.append(j)
        else:
            summary.append(item)

    # add cycle number to summary dict
    # print(summary)
    if summary:
        cycle = 1
        operation = 1
        for record in summary:
            record['Cycle'] = cycle
            if operation % 2:
                operation += 1
            else:
                operation += 1
                cycle += 1
        print("Total number of cycles: ", cycle)
    return summary


class FetchDataError(Exception):
    pass


class FlagOverwriteException(Exception):
    pass
