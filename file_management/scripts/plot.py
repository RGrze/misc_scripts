import os
from datetime import datetime
from itertools import cycle
import warnings
# import sys

from bokeh.layouts import column
from bokeh.models import CustomJS, RangeSlider, Slider, HoverTool, Label, CustomJSFilter, CDSView
from bokeh.plotting import figure, ColumnDataSource
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh import palettes
import pandas as pd
from numpy import polyfit, RankWarning


class Plotter:
    def __init__(self, **kwargs):
        """
        kwargs:
            sample: Sample object
            summary_df: pd.DataFrame
            address: str, path to sample folder if Sample object was not specified
        """
        self.sample = kwargs.pop('sample', None)
        self.summary_df = kwargs.pop('summary_df', None)
        self.address = kwargs.pop('address', None)

    def run(self):
        if self.address:
            adr = self.address
            self.samplename = os.path.split(adr)[1]
        elif self.sample:
            adr = self.sample.address
            self.samplename = self.sample.normalized_fn
        else:
            raise NoSampleError("No sample was provided")

        if not isinstance(self.summary_df, pd.DataFrame):
            for file in [f for f in os.listdir(adr) if os.path.isfile(os.path.join(adr, f))]:
                if f'{self.samplename}.csv'.lower() == file.lower():
                    path = os.path.join(adr, file)
                    try:
                        self.summary_df = pd.read_csv(path, sep='\t').set_index("Cycle")
                    except FileNotFoundError:
                        print(f"Couldn't load summary dataframe for {self.samplename}")
                        self.summary_df = None
        # get each log file:
        if os.path.isdir(os.path.join(adr, 'raw_data')):
            _adr = os.path.join(adr, 'raw_data')
            self.files = [os.path.join(_adr, f) for f in os.listdir(_adr) if os.path.isfile(os.path.join(_adr, f))]
        else:
            self.files = []

        plot_dir = os.path.join(adr, 'plots')
        if not os.path.isdir(plot_dir):
            os.makedirs(plot_dir)

        self.plot_directory = plot_dir

        if isinstance(self.summary_df, pd.DataFrame):
            self.plot_summary()
        if self.files:
            self.plot_voltage_vs_capacity()

    # SUMMARY PLOT - CAPACITY VS CYCLES
    def plot_summary(self):
        df = self.summary_df.assign(color=['red' if type == 'charge' else 'blue'
                                           for type in self.summary_df['Measurement type']],
                                    marker=['triangle' if type == 'charge' else 'inverted_triangle'
                                           for type in self.summary_df['Measurement type']]
                                    )
        rows_to_multiply = ['Total capacity', 'Start current', 'Stop current']
        # Get current and capacity in uA / uAh instead of mA / mAh
        for row in rows_to_multiply:
            df[row] *= 1000

        source = ColumnDataSource(df)
        # setup figure:
        fig = self._get_figure("Capacity vs Cycles")
        fig.scatter(x="Cycle", y="Total capacity", source=source, marker="marker",
                    size=7, color='color', name='needshover')
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                x, y, slope, intercept = self._get_degradation_rate(df)
                fit_source = ColumnDataSource({'x': x, 'y': y})
                annotation = Label(x=10, y=0, x_units='screen', y_units='screen', # bottom left corner of plot
                                   text=f'y = {slope:.3f}*x + {intercept:.3f}, deg. rate: {(slope / intercept) * 100:.4f} %',
                                   render_mode='css', text_font_size='14pt')
                fig.add_layout(annotation)

                # Configure sliders:
                # ###Range slider:
                range_slider = RangeSlider(start=1, end=x[-1], value=(1, x[-1]), step=1, title="Range")

                callback = self._polyfit_callback_capacity_vs_cycles(source, fit_source, range_slider, annotation)
                range_slider.js_on_change('value', callback)
                fig.line(x='x', y='y', source=fit_source)  # , view=view)
                layout = column(fig, range_slider)  # , fit_slider)
            except (TypeError, RuntimeWarning, RankWarning):
                layout = fig
            self._save_fig(layout, "Capacity vs Cycles")


        # # ###Fit length slider:
        # if x[-1] < 50:
        #     dots = 100
        # else:
        #     dots = x[-1] * 1.5
        # fit_slider = Slider(start=1, end=dots, value=dots, step=1, title="Fit length")
        #
        # fit_line_filter = CustomJSFilter(args=dict(slider=fit_slider, source=fit_source),
        #                                  code="""
        #                             line_length = slider.value; // get slider value
        #                             return [...Array(line_length).keys()]; // create array with indices from 0 to slider value
        #                             """)
        # update_fit_line = CustomJS(args=dict(source=fit_source),
        #                            code="""
        #                                 source.change.emit();
        #                                 """)
        # fit_slider.js_on_change('value', update_fit_line)
        # view = CDSView(source=fit_source, filters=[fit_line_filter])

    def plot_voltage_vs_capacity(self):
        fig = self._get_figure("Voltage vs Capacity")
        # create palette with proper number of colors:
        total_num_of_cycles = 0
        for file in self.files:
            if file.endswith('.xls'):
                _df = pd.read_excel(file, -1)
                total_num_of_cycles += _df.iloc[-1, 3]
            else:
                total_num_of_cycles += 1
        self.colors = Colors(total_num_of_cycles)

        for file in self.files:
            if file.endswith('.csv'):
                self._add_line_from_csv_voltage_vs_capacity(fig, file)
            elif file.endswith('.xls'):
                self._add_line_from_xls_voltage_vs_capacity(fig, file)

        self._save_fig(fig, "Voltage vs Capacity")

    def _add_line_from_csv_voltage_vs_capacity(self, fig, file):
        df = pd.read_csv(file, sep='\t')
        # date = re.findall(r'^([0-9]{4}\.[0-9]{2}\.[0-9]{2}\-\-[0-9]{2}\.[0-9]{2}\.[0-9]{2}).*', file)

        date = os.path.split(file)[1].split('_')[0]
        try:
            date = datetime.strptime(date, "%Y.%m.%d--%H.%M.%S")
        except ValueError:
            date = datetime.strptime(date, "%Y.%m.%d-%H.%M.%S")

        df["Date"] = date

        measurement_type = os.path.split(file)[1].split('_')[-2]
        df["Measurement type"] = measurement_type

        operation_num = int(os.path.split(file)[1].split('_')[-1].replace('.csv', ''))
        cycle_num = (operation_num + 1) // 2 if operation_num % 2 else operation_num // 2
        color = self.colors.current if operation_num % 2 else next(self.colors)
        df["Cycle"] = cycle_num

        try:
            df['Current']
        except KeyError:
            df['Current'] = round(df["Capacity"] / (df["Time"] / 3600), 5)
        finally:
            mode = 'CCCV' if df['Current'].iloc[0] == df['Current'].iloc[-1] else 'CC'
            df['Measurement mode'] = mode
            df['Current'] *= 1000
            df['Capacity'] *= 1000

        source = ColumnDataSource(df)
        fig.line(x='Capacity', y='Voltage', source=source, line_color=color, name=str(cycle_num))

    def _add_line_from_xls_voltage_vs_capacity(self, fig, file):
        # def convert_to_seconds(data: str) -> float:
        #     data = data.replace(':', '.').split('.')
        #     return int(data[0]) * 3600 + int(data[1]) * 60 + int(data[2]) + (int(data[3]) / 10E2)
        xls = pd.ExcelFile(file)
        data_sheets = [sheet for sheet in xls.sheet_names if 'Detail' in sheet]

        df_list = []
        for sheet in data_sheets:
            df = pd.read_excel(xls, sheet)
            df_list.append(df)

        df = pd.concat([df for df in df_list], ignore_index=True)
        df = df.iloc[:, [1, 3, 5, 6, 7, 9, 10]].copy()

        df.rename(columns={df.columns[2]: "Current",
                           df.columns[3]: "Voltage",
                           df.columns[4]: "Capacity",
                           df.columns[5]: "Time",
                           df.columns[6]: "Date"},
                  inplace=True)

        # df["Time"] = [convert_to_seconds(x) for x in df["Time"]]
        df["Current"] *= 1000
        df["Capacity"] *= 1000

        last_cycle = df['Cycle'].iloc[-1]
        operation = 1
        cycle = 1
        while cycle < last_cycle:
            if operation % 2:  # operation nr 1 3 5 etc
                df_ = df.query(f"Status.str.contains('DChg|Rest') == False & Cycle == '{cycle}' ").copy()
                color = next(self.colors)
            else:  # operation nr 2 4 6 etc
                df_ = df.query(f"Status.str.contains('DChg') & Cycle == '{cycle}' ").copy()
                color = self.colors.current
                cycle += 1

            df_["Measurement mode"] = df_['Status'].iloc[0].split('_')[0]
            df_["Measurement type"] = "charge" if df_['Status'].iloc[0].split('_')[1] == "Chg" else "discharge"
            source = ColumnDataSource(df_)
            fig.line(x='Capacity', y='Voltage', source=source, line_color=color, name=str(cycle))

            operation += 1

    def _save_fig(self, fig, plot_type):
        filename = os.path.join(self.plot_directory, f'{self.samplename}_{plot_type}.html')
        html = file_html(fig, CDN, f'{self.samplename} {plot_type}').encode("utf-8")
        with open(filename, 'wb+') as f:
            f.write(html)

    def _get_degradation_rate(self, df):
        x = [val for val in df[df['Measurement type'] == 'discharge'].index]
        y = [val for val in df[df['Measurement type'] == 'discharge']['Total capacity']]
        slope, intercept = polyfit(x, y, 1)

        fit_y = [slope * xi + intercept for xi in x]
        return x, fit_y, slope, intercept

    def _get_figure(self, title):
        """
        Initialize and configure figure for capacity vs cycles plots
        """
        # Create figure:
        fig = figure(plot_width=1000, plot_height=800,
                     title=f"{self.samplename} - {title}")
        # Get hover tool:
        if title == 'Capacity vs Cycles':
            hover_tool = self._hover_tool_capacity_vs_cycles()
            fig.xaxis.axis_label = 'Cycle number, #'
            fig.yaxis.axis_label = 'Capacity, uAh'
        else:
            hover_tool = self._hover_tool_voltage_vs_capacity()
            fig.xaxis.axis_label = 'Capacity, uAh'
            fig.yaxis.axis_label = 'Voltage, V'

        fig.title.text_font_size = "14pt"
        fig.xaxis.axis_label_text_font_size = "18pt"
        fig.xaxis.major_label_text_font_size = "16pt"
        fig.yaxis.axis_label_text_font_size = "18pt"
        fig.yaxis.major_label_text_font_size = "16pt"
        fig.add_tools(hover_tool)
        return fig

    def _hover_tool_capacity_vs_cycles(self):
        return HoverTool(
            names=['needshover'],
            tooltips=[
                ("Date", "@Date{%Y-%m-%d %H:%M}"),
                ("Mode", "@{Measurement mode}"),
                ("Cycle", "@Cycle"),
                ("Start Current (uA)", "@{Start current}{0.000}"),
                ("Finish Current (uA)", "@{Stop current}{0.000}"),
                ("Capacity (uAh)", "@{Total capacity}{0.000}"),
                ("CV / CC ratio", "@{CV/CC}{0.000}")
            ],
            formatters={'Date': 'datetime'}
        )

    def _hover_tool_voltage_vs_capacity(self):
        return HoverTool(
            tooltips=[
                ("Date", "@Date{%Y-%m-%d %H:%M}"),
                ("Mode", "@{Measurement mode}"),
                ("Meas. type", "@{Measurement type}"),
                ("Cycle", "@Cycle{0}"),
                ("Current (uA)", "@{Current}{0.000}"),
                # ("Capacity (uAh)", "@Capacity{0.000}"),
                ("(x, y)", "$x, $y")
            ],
            formatters={'Date': 'datetime'}
        )

    def _polyfit_callback_capacity_vs_cycles(self, source, source_line_fit, range_slider, annotation):
        """
        create callback that computes new linear coeffs and deg rate when range slider is changed
        """
        return CustomJS(args=dict(s=source, s1=source_line_fit,
                                  range_slider=range_slider, annotation=annotation),
                            code="""
                            var data = s.data;
                            var data1 = s1.data;
                            var slider_range = range_slider.value;
                            var y1 = data1['y'];
                            var x1 = data1['x'];
                            var x2 = [];
                            var y2 = [];
                            
                            for (var i=0, len=data['Cycle'].length; i < len; i++){
                                if (data['Measurement type'][i] == "discharge"){
                                    x2.push(data['Cycle'][i]);
                                    y2.push(data['Total capacity'][i]);
                                }
                            }
        
                            var fit_range_x = x2.slice(slider_range[0], slider_range[1]+1);
                            var fit_range_y = y2.slice(slider_range[0], slider_range[1]+1);
                            
                            console.log(fit_range_x, fit_range_y);
                            function findLineByLeastSquares(values_x, values_y) {
                                var x_sum = 0;
                                var y_sum = 0;
                                var xy_sum = 0;
                                var xx_sum = 0;
                                var count = 0;

                                /*
                                 * The above is just for quick access, makes the program faster
                                 */
                                var x = 0;
                                var y = 0;
                                var values_length = values_x.length;

                                if (values_length != values_y.length) {
                                    throw new Error('The parameters values_x and values_y need to have same size!');
                                }

                                /*
                                 * Above and below cover edge cases
                                 */
                                if (values_length === 0) {
                                    return [ [], [] ];
                                }

                                /*
                                 * Calculate the sum for each of the parts necessary.
                                 */
                                for (let i = 0; i< values_length; i++) {
                                    x = values_x[i];
                                    y = values_y[i];
                                    x_sum+= x;
                                    y_sum+= y;
                                    xx_sum += x*x;
                                    xy_sum += x*y;
                                    count++;
                                }

                                /*
                                 * Calculate m and b for the line equation:
                                 * y = x * m + b
                                 */
                                var sl = (count*xy_sum - x_sum*y_sum) / (count*xx_sum - x_sum*x_sum);
                                var interc = (y_sum/count) - (sl*x_sum)/count;

                                return [sl, interc]
                            }
                            var [slope, intercept] = findLineByLeastSquares(fit_range_x, fit_range_y);
                            line_length = data['Cycle'].length * 0.5
                            for (var i = 0; i < line_length; i++) {         
                                y1[i] = slope * i + intercept;
                                x1[i] = i;
                            }
                            annotation.text = "y = " + String(slope.toFixed(4)) + "*x + " + String(intercept.toFixed(4)) +
                                              "; deg. rate = " + ((slope / intercept) * 100).toFixed(3) + " %";  
                            s1.change.emit();
                        """)


class Colors:
    """ Wrapper for palette iterator that store current iterator's value"""
    def __init__(self, n, palette=palettes.plasma):
        palette_length = round((n + 1) / 2, 0)
        if palette_length >= 255:
            palette_length = 255
        self.palette = cycle(palette(palette_length)[::-1])
        self.current = next(self.palette)

    def __next__(self):
        self.current = next(self.palette)
        return self.current


class NoSampleError(Exception):
    pass