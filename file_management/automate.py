from datetime import datetime, timedelta
import shutil
from distutils.dir_util import copy_tree, remove_tree
import os
import time

import pandas as pd

from scripts.sample import Sample
from scripts import plot, data

DAYS_WITHOUT_MODIFICATION = 1
FILES_NOT_OLDER_THAN = 15  # days
FILES_NOT_NEWER_THAN = 1  # days

adr = os.path.normpath(r'\\192.168.0.250\roman\Measurements\cycle')

# TODO: join plotting to the loop
#       add multiprocessing to speed up script?

def run(root_adr=adr, **kwargs):
    """Main function that runs automated plots.

    args:
        root_adr (str): path to main sample folder (i.e. '~/Documents/Measurements' if you want to cycle through
            all samples inside Measurements folder. You can also use plotter with single
            sample, just provide path to specific sample i.e. '~Documents/Measurements/Sample_1'

    Keyword arguments:
        battery_df_adr (str): custom path to CSV file with all measurements
        move (bool): select if you want to move files to normalized folder, by default it's True
        chinese_stand_adr (str): custom path to folder with chinese stand data. It's only necessary
            if :move: kwarg was provided.
        single (bool): Select if you want to perform only one plot, or it should do it in infinite loop
        sleep_time (float): How long the script should wait (in HOURS), before next loop cycle will be perofmed.
            Works only if :single: is False
        exception_adr (str): you can provide custom path to exceptions folder (samples
            that doesn't fit naming pattern ought be placed inside exceptions)
        keithleyfile (bool): select True if logs are from Keithley 2400SM. These logs have current in A, that
            causes wrong Start/Finish current values on Plotter's hover tool. Default: False
    """
    exception_adr = kwargs.pop('exception_adr', os.path.join(root_adr, 'exceptions'))
    chinese_stand_adr = kwargs.pop('chinese_stand_adr', os.path.normpath(r'C:\Users\neware\Desktop\BackUp'))
    battery_df_adr = kwargs.pop('battery_df_adr', os.path.join(root_adr, 'battery db.csv'))
    move = kwargs.pop('move', True)
    single = kwargs.pop('single', False)
    sleep_time = kwargs.pop('sleep_time', 6)
    keithleyfile = kwargs.pop('keithleyfile', False)

    while True:
        # move samples from main folder, that meet conditions:
        # 1. Receive samples from main folder and chinese folder
        if move:
            samples_to_move = get_samples_to_move(root_adr, chinese_stand_adr)
            for sample in samples_to_move:
                move_files(sample, root_adr, exception_adr)

        # get list of all sample folders excluding these from exclude folders
        exclude = {'exceptions', 'Dead and commercial batteries'}
        sample_dirs = []
        for root, subdirs, files in os.walk(root_adr):
            subdirs[:] = [d for d in subdirs if d not in exclude]
            if 'raw_data' in subdirs:
                now = datetime.now()
                m_time = datetime.fromtimestamp(os.path.getmtime(os.path.join(root, subdirs[subdirs.index('raw_data')])))
                if m_time >= now - timedelta(days=FILES_NOT_OLDER_THAN):  # it's unnecesary to loop every old folder every time
                    sample_dirs.append(root)
        samples = []
        for sample in sample_dirs:
            try:
                s = Sample(sample, keithleyfile=keithleyfile)
            except:
                continue
            samples.append(s)

        for sample in samples:
            print(sample.normalized_fn)
            summary = data.create_sample_summary(sample)
            if not summary:
                continue
            try:
                summary_df = pd.DataFrame(summary).set_index('Cycle')
                summary_df.to_csv(
                    os.path.join(sample.address, f'{sample.normalized_fn}.csv'), sep='\t')
            except KeyError:
                print(f"pandas to csv KeyError - sample: {sample.normalized_fn}")
                continue
            try:
                data.update_batteries_database(battery_df_adr, sample, summary)
            except data.FlagOverwriteException:
                continue
            try:
                plot.Plotter(sample=sample, summary_df=summary_df).run()
            except plot.NoSampleError:
                continue
            # plotter.run()  # plot Capacity vs Cycles and Voltage vs Capacity for sample
        if single:
            break
        else:
            # sleep for 6 hours
            t = datetime.fromtimestamp(time.time() + sleep_time * 3600)
            print(f"Sleeping until {t.hour:02d}:{t.minute:02d}:{t.second:02d}")
            time.sleep(sleep_time * 3600)


def get_samples_to_move(address: str, chinese_stand_folder: str = None) -> list:
    """
        This function returns a list of samples that are placed in main measurements folder / chinese stand folder
            and were last modified between set range (it causes to not loop through whole measurement
            folder every time)
    """
    paths = []
    # deal with samples in main folder:
    folders = [f for f in os.listdir(address) if os.path.isdir(os.path.join(address, f, 'raw_data'))]
    now = datetime.now()
    not_older_than = now - timedelta(days=FILES_NOT_OLDER_THAN)
    not_newer_than = now - timedelta(days=FILES_NOT_NEWER_THAN)
    for sample_folder in folders:
        m_time = datetime.fromtimestamp(os.path.getmtime(os.path.join(address, sample_folder, 'raw_data')))
        if not_newer_than >= m_time >= not_older_than:
            paths.append(os.path.join(address, sample_folder))
    # get files from china stand
    if chinese_stand_folder:
        files = [f for f in os.listdir(chinese_stand_folder) if f.endswith('xls')]
        for file in files:
            m_time = datetime.fromtimestamp(os.path.getmtime(os.path.join(chinese_stand_folder, file)))
            if not_newer_than >= m_time >= not_older_than:
                paths.append(os.path.join(chinese_stand_folder, file))

    samples = []
    for p in paths:
        try:
            s = Sample(p)
        except:
            continue
        samples.append(s)
    return samples


def remove_sample_duplicates(samples: list) -> list:
    """filter samples so the list contains only one sample's object (many objects can be created from multiple files,
    but they're moved to normalized directory with move.files1() function"""
    seen = []
    samples_ = []
    for sample in samples:
        if sample.normalized_fn not in seen:
            seen.append(sample.normalized_fn)
            samples_.append(sample)
    return samples_


def move_files(sample: Sample, root_dst_adr, exception_dst_adr=None):

    if sample.exception:
        print(f'{sample.normalized_fn} moved to exceptions folder')
        if not exception_dst_adr:
            exception_dst_adr = os.path.join(root_dst_adr, 'exceptions')
        dst_sample_dir = os.path.join(exception_dst_adr, sample.cathode_name, sample.normalized_fn)
    else:
        dst_sample_dir = os.path.join(root_dst_adr, sample.cathode_name, sample.normalized_fn)

    if dst_sample_dir == sample.address:
        raise IsADirectoryError("Source path is the same as destination path")
    print(f'Moving {sample.address} -> {dst_sample_dir} ...')

    if not os.path.exists(os.path.join(dst_sample_dir, 'raw_data')):
        os.makedirs(os.path.join(dst_sample_dir, 'raw_data'))

    if os.path.isfile(sample.address):  # this case should occur only for xls files in china stand cycle folder
        dst = os.path.join(dst_sample_dir, 'raw_data', sample.path_tail)
        # unnecesary?
        if dst == sample.address:
            raise IsADirectoryError("Source path is the same as destination path")
        ##
        if os.path.exists(dst):
            if not os.path.samefile(sample.address, dst):
                os.remove(dst)
        try:
            shutil.move(sample.address, dst)
        except PermissionError:
            shutil.copy2(sample.address, dst)
    else:  # case where files are searched in sample folder
        # TODO: all this files moving probably could've been done in more clear way...
        # log files have priority so we move them separately first
        files = [f for f in os.listdir(os.path.join(sample.address, 'raw_data'))]
        for file_ in files:
            src = os.path.join(sample.address, 'raw_data', file_)
            dst = os.path.join(dst_sample_dir, 'raw_data', file_)
            if os.path.exists(dst):
                if not os.path.samefile(src, dst):
                    os.remove(dst)
            shutil.move(src, dst)
        os.rmdir(os.path.join(sample.address, 'raw_data'))
        main_folder_files = [f for f in os.listdir(sample.address) if
                             os.path.isfile(os.path.join(sample.address, f))]
        for file in main_folder_files:
            shutil.move(os.path.join(sample.address, file), os.path.join(dst_sample_dir, file))

        rest_folders = [f for f in os.listdir(sample.address) if
                        os.path.isdir(os.path.join(sample.address, f))]
        for folder in rest_folders:
            copy_tree(os.path.join(sample.address, folder), os.path.join(dst_sample_dir, folder))

        remove_tree(sample.address)


if __name__ == '__main__':
    run()
