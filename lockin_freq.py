# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 10:07:01 2018

@author: Measurement
"""
import sys
import time
import shutil
import csv
import os.path
import visa
import math
import numpy as np

### mail
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
###

measurement_folder = os.path.expanduser("~/Documents/Measurements/lock-in/")
rm = visa.ResourceManager()



def run(sample_ID, minimum, maximum, steps, sendmail=False):
    srs = rm.open_resource('USB0::0xB506::0x2000::003398::INSTR')
    """main function"""
    print(f"sample ID : {sample_ID}\nfinish frequency: {minimum} Hz\nstart frequency: {maximum}Hz\nsteps: {steps}")
    ###
    #  file management:
    store_folder = os.path.join(measurement_folder, sample_ID)
    ampl = float(srs.query('SLVL?'))
    filename = set_filename(sample_ID, ampl, store_folder)
    full_path = os.path.join(store_folder, filename)
    fieldnames = ['frequency', 'time', 'X', 'Y', 'R', 'deg']
    create_file(full_path, fieldnames)
    ###
    frequency = maximum
    dB_step = get_step_in_dB(minimum, maximum, steps)
    frequency_step = 10**(dB_step / 10)
    #time_pauses = (
    #	10**(-5), 3*10**(-5), 10**(-4), 3*10**(-4),
    #	10**(-3), 3*10**(-3), 10**(-2), 3*10**(-2),
    #	10**(-1), 3*10**(-1), 1, 3, 10, 30, 100, 300,
    #   1000, 3000, 10000, 30000, 100000, 300000
    #)
    time_pauses = (
            1 * 10**(-6), 3 * 10**(-6), 10 * 10**(-6), 3 * 10 * 10**(-6),
            100**(-6), 3 * 100**(-6), 10**(-3), 3 * 10**(-3),
            10**(-3), 3 * 10**(-3), 100**(-3), 3 * 100**(-3),
            1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000
    )
    time_pauses = np.array(time_pauses)*10
    k = 0
    p_accuracy = 1
#    phases = np.empty(30)
    data = np.empty([4, 30])
    srs.write('OFLT 0')
    time.sleep(1)
    start_time = time.time()
    while frequency >= minimum / frequency_step:
        srs.write('FREQ {}'.format(frequency))
        print("Current frequency: {}".format(frequency))
        m_accuracy = 180
        while m_accuracy > p_accuracy:
#            print("sleep: {} s".format(time_pauses[k]))
            time.sleep(time_pauses[k])
            if k == 21:
                break
            for j in range(30):
                if 1 / frequency > time_pauses[k]:
                    time.sleep(1 / (26 * frequency) + 0.05)
#                    phases[j] = srs.query('OUTP? 3')
                    data[0][j], data[1][j], data[2][j], data[3][j] = srs.query('SNAPD?').split(',')
                else:
                    time.sleep(time_pauses[k] / 60 + 0.05)
#                    phases[j] = srs.query('OUTP? 3')
                    data[0][j], data[1][j], data[2][j], data[3][j] = srs.query('SNAPD?').split(',')
            m_accuracy = np.std(data[3])
            # print("std: {:4f}".format(m_accuracy))
            if m_accuracy > p_accuracy and k < 21:
                k += 1
                srs.write('OFLT {}'.format(k))
            elif m_accuracy <= p_accuracy:
                if k != 0:
                    k -= 1
                srs.write('OFLT {}'.format(k))
                measurement = [np.mean(array) for array in data]
                read_time = time.time() - start_time
                save_data(full_path, fieldnames, frequency, measurement, read_time)
                srs.write('ASCL')
#        measurement = srs.query('SNAPD?').split(',')  # get a read of X,Y,R,DEG
        
                frequency /= frequency_step
    # srs.close()
    if sendmail:
        sendMail(full_path+'.csv')


def get_step_in_dB(start_frequency, finish_frequency, steps):
    finish_dB = 10 * math.log(finish_frequency / start_frequency, 10)
    return finish_dB / steps


def save_data(full_path, fieldnames, frequency, lista, read_time):
    """append .csv file with last record"""
    data = {'frequency': frequency, 'time': read_time, 'X': lista[0], 'Y': lista[1],
            'R': lista[2], 'deg': float(lista[3])}
    with open(full_path + '.csv', 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
        writer.writerow(data)


def create_file(full_path, fieldnames):
    """create new .csv file"""
    if not os.path.exists(os.path.dirname(full_path)):
        os.makedirs(os.path.dirname(full_path))
    with open(full_path + '.csv', 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()


def set_filename(sample_ID, ampl, store_folder):
    """set filename"""
    t = time.strftime("%Y.%m.%d")
    if os.path.exists(store_folder):
        number_of_file = len(next(os.walk(store_folder))[2])+1
    else:
        number_of_file = 1
    filename = "{}_{}_{:.3f} mV-{}".format(sample_ID, t, ampl*1000, number_of_file)
    return filename

def upload_to_server(file, server_file_path='//192.168.0.250//roman//Measurements//lock-in'):
    """Upload file to local server
    Args:
        file: str
            full directory to a file e.g. "~/Documents/file.txt"
        server_file_path: str
            directory in local server where files should be uploaded,
            e.g. "//192.168.0.1/folder1/folder2/"
    """
    try:
        if not os.path.exists(server_file_path):
            os.makedirs(server_file_path)
        shutil.copy2(file, server_file_path)
    except OSError as e:
        print(e)

def sendMail(file):
    """
    function used for sending an email with measurement log to recipients
    Args:
        kwargs:
            sampleID: str
            measurement_type: str
            date: str
            directory: str
            ''
    """
    try:

        fromaddr = "roman.grzesiakowski@thebatteries.org"
        toaddr = fromaddr
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        # Subject: "[SAMPLE ID] - [MEASUREMENT TYPE] from [DATE] data "
        msg['Subject'] = "Lock-in data"

        body = ''
        msg.attach(MIMEText(body, 'plain'))
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(file, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=file)
        msg.attach(part)
        server = smtplib.SMTP('smtp.zoho.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(fromaddr, 'VqxVAitHNHJh')
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print("Email sent")
    except (smtplib.SMTPRecipientsRefused, smtplib.socket.gaierror) as error:
        print(error)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        minimum = float(sys.argv[2])
        maximum = float(sys.argv[3])
        steps = int(sys.argv[4])
        run(filename, minimum, maximum, steps)
    else:
        run('MAG291119_H2_s2', 10, 400E3, 150)
