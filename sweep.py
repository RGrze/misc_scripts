import visa
import os
import time
from pandas import read_csv
import matplotlib.pyplot as plt

measurement_folder = os.path.expanduser("~/Documents/Measurements/current_scanner")
rm = visa.ResourceManager()

def init_device(src, start, limit):
    keithley = rm.open_resource('ASRL2::INSTR')
    
    keithley.write('*RST')
    keithley.write(':SYSTem:TIME:RESet')  # resets device's timestamp
    keithley.write(':OUTPUT:SMODE HIMP')
    keithley.write(':SYST:RSENSE OFF')
    if src == 'V':
        keithley.write(':SOURCE:FUNCTION VOLTAGE')  # set source to voltage
        keithley.write(':SOURCE:VOLTAGE:MODE FIXED')
        keithley.write(f':SOUR:VOLTAGE:RANG {start}')  # range of voltage
        keithley.write(f':SOUR:VOLTAGE:LEV {start}')
        keithley.write(":SENS:FUNC 'CURRENT'")  # set measure function to current
        keithley.write(f':SENS:CURRENT:PROT {limit}')  # 1A current compliance
        # keithley.write(':SENS:CURRENT:RANG {}'.format(current_compliance))  # 1A measure current range
        keithley.write('SENS:CURRENT:RANGE:AUTO ON')  # AUTO range for current
    else:
        keithley.write(':SOURCE:FUNCTION CURRENT')  # set source to voltage
        keithley.write(':SOURCE:CURRENT:MODE FIXED')
        keithley.write(f':SOUR:CURRENT:RANG {start}')  # range of voltage
        keithley.write(f':SOUR:CURRENT:LEV {start}')
        keithley.write(":SENS:FUNC 'VOLTAGE'")  # set measure function to current
        keithley.write(f':SENS:VOLTAGE:PROT {limit}')  # 1A current compliance
        # keithley.write(':SENS:CURRENT:RANG {}'.format(current_compliance))  # 1A measure current range
        keithley.write('SENS:VOLTAGE:RANGE:AUTO ON')  # AUTO range for current
    keithley.write(':FORM:ELEM VOLT,CURR')  # measures time,voltage,current
    return keithley


def run(sample, runs, source, start, finish, step_incr, limit, reverse=True):
    keithley = init_device(source, start, limit)
    if source == 'V':
        file = f'{sample}_sweep_{start} V - {finish} V limit {limit} A.csv'
    else:
        file = f'{sample}_sweep_{start} A - {finish} A limit {limit} V.csv'
    if os.path.isfile(os.path.join(measurement_folder, sample, file)):
        i = 1
        file_ = file[:-4] + f'({i})' + file[-4:]
        while os.path.isfile(os.path.join(measurement_folder, sample, file_)):
            i += 1
            file_ = file[:-4] + f'({i})' + file[-4:]
        file = file_
    if not os.path.isdir(os.path.join(measurement_folder, sample)):
        os.makedirs(os.path.join(measurement_folder, sample))

    file_path = os.path.join(measurement_folder, sample, file)
    with open(file_path, 'w+') as f:
        f.write("Voltage (V)\tCurrent (A)\tTime (s)\n")

    st = time.time()
    for i in range(runs):
        loop(keithley, source, start, finish, step_incr, limit, st, file_path)
        if reverse:
            loop(keithley, source, finish, start, step_incr, limit, st, file_path)

    plt.ioff()
    read_csv(file_path, sep='\t').plot(0, 1, title=file[:-4]).get_figure()\
    .savefig(os.path.join(os.path.split(file_path)[0], file[:-4]+'.jpg'))

def loop(keithley, source, start, finish, step_incr, limit, start_time, file_path):
    start_ = start
    limit_counter = 0
    keithley.write(":OUTP ON")
    while True:
        volt, current = [float(x) for x in keithley.query(":READ?").split(',')]
        read_time = time.time() - start_time

        if start > finish:
            if start_ < finish:
                break
            start_ -= step_incr

        else:
            if start_ > finish:
                break
            start_ += step_incr


        if source == 'V':
            keithley.write(f':SOUR:VOLTAGE:RANG {start_}')  # range of voltage
            keithley.write(f":SOUR:VOLTAGE:LEV {start_}")
        else:
            keithley.write(f':SOUR:CURRENT:RANG {start_}')  # range of voltage
            keithley.write(f":SOUR:CURRENT:LEV {start_}")

        # if source == 'V':
        #     if abs(round(current, 2)) >= limit:
        #         if limit_counter == 0:
        #             limit_counter += 1
        #             pass
        #         else:
        #             continue
        # else:
        #     if abs(round(volt, 2)) >= limit:
        #         if limit_counter == 0:
        #             limit_counter += 1
        #             pass
        #         else:
        #             continue
        with open(file_path, 'a') as f:
            f.write(f"{volt}\t{current}\t{read_time}\n")
    keithley.write(":OUTP OFF")

if __name__ == '__main__':
    run('TFB18122019_H4_s1', runs=1, source='V', start=-4.2, finish=4.2, step_incr=0.025, limit=10e-6, reverse=True)
