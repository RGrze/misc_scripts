import os

import numpy as np
import matplotlib.pyplot as plt


def run(sample_file, clean_mica_file, light_file=None, dark_file=None):
    sample_head, sample_tail = os.path.split(sample_file)
    light_wavelength, light_counts = read_file(light_file)
    sample_wavelength, sample_counts = read_file(sample_file)
    clean_mica_wavelength, clean_mica_counts = read_file(clean_mica_file)

    if dark_file:
        dark_wl, dark_counts = read_file(dark_file)
        light_counts -= dark_counts
        sample_counts -= dark_counts
        clean_mica_counts -= dark_counts

    clean_mica_transmission = clean_mica_counts / light_counts
    sample_transmission = sample_counts / clean_mica_transmission

    result = sample_transmission * 100

    fig, ax = plt.subplots()
    ax.plot(result[40:-20], result[40:-20])
    ax.set_xlabel("Wavelength, nm")
    ax.set_ylabel("Transmission, %")
    ax.set_title(sample_tail)
    plot_filename = sample_tail[:-4] + '.jpg'
    plt.savefig(os.path.join(sample_head, plot_filename))

def read_file(file):
    wavelength = []
    counts = []
    with open(file, 'r') as source:
        leave_lines = 0
        for line in source:
            leave_lines += 1
            if leave_lines > 8 and line != '\n':
                data = line.replace(',', '.').split(';')
                wavelength.append(float(data[0]))
                counts.append(float(data[1]))
    return np.array(wavelength), np.array(counts)
