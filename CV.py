# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 09:47:38 2019

@author: Measurement
"""

import sys
import os
import time

import visa


rm = visa.ResourceManager()
device = rm.open_resource('ASRL2::INSTR')

measurement_directory = os.path.expanduser('~/Documents/Measurements/sweep/')


def sweep_settings(start_voltage, current_compliance=4000e-6, sense_mode='2W'):
    device.write('*RST')
    device.write(':SYSTem:TIME:RESet')  # resets device's timestamp
    device.write(':OUTPUT:SMODE HIMP')
    if sense_mode == '4W':
        device.write(':SYST:RSENSE ON')
    else:
        device.write(':SYST:RSENSE OFF')
    device.write(':SOURCE:FUNCTION VOLTAGE')  # set source to voltage
    device.write(':SOURCE:VOLTAGE:MODE FIXED')
    device.write(':SOUR:VOLTAGE:RANG {}'.format(start_voltage))  # range of voltage
    device.write(':SOUR:VOLTAGE:LEV {}'.format(start_voltage))
    device.write(":SENS:FUNC 'CURRENT'")  # set measure function to current
    device.write(':SENS:current:PROT {}'.format(current_compliance))  # 1A current compliance
    # device.write(':SENS:CURRENT:RANG {}'.format(current_compliance))  # 1A measure current range
    device.write('SENS:CURRENT:RANGE:AUTO ON')  # AUTO range for current
    device.write(':FORM:ELEM VOLT,CURR')  # measures time,voltage,current
    print("Sweep settings loaded")


def run(ID, start_voltage, trigger_drop, delay, sense_mode='2W'):
    # print(start_voltage, stop_voltage, step)
    date = time.strftime("%Y.%m.%d--%H.%M.%S")
    filename = "{}_sweep_{}-V_{}".format(ID, start_voltage, date)
    full_path = os.path.join(measurement_directory, ID, filename + '.csv')

    create(full_path)
    write(full_path, ['Voltage', 'Current', 'Time'])

    sweep_settings(start_voltage, sense_mode=sense_mode)
    start_time = time.time()

    device.write(":OUTP ON")
    data = read(start_time)
    write(full_path, data)
    first_current = data[1]
    while True:
        data = read(start_time)
        write(full_path, data)
        if data[2] > 43200*2:  # 43200 seconds = 12hours
            break
        elif first_current / trigger_drop > data[1]:
            break
        time.sleep(delay)
    device.write(":OUTP OFF")



def read(start_time):
    measurement = device.query('READ?').split(',')
    read_time = time.time() - start_time
    return (measurement[0], abs(float(measurement[1])), read_time)


def create(directory):
    """Create empty file

    Args:
        directory: str
            full directory to the file, e.g. /home/pi/Documents/file.csv
    """
    path_folder = os.path.split(directory)[0]
    if not os.path.isdir(path_folder):
        os.makedirs(path_folder)
    with open(directory, 'x') as f:
        f.close()


def write(directory, data, sep='\t', single_line=True):
    """Write headers to the file, tab separator as default

    Args:
        directory: str
            full directory to the file, e.g. /home/pi/Documents/file.csv
        header: list
            list of headers
        sep: str
            separator between columns
        single_line: bool
            True: write array in row
            False: write array in column
    """
    with open(directory, 'a', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        if single_line:
            writer.writerow(data)
        else:
            writer.writerows(zip(data))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        start = float(sys.argv[2])
        trigger_drop = int(sys.argv[3])
        delay = int(sys.argv[4])
        sense_mode = str(sys.argv[5])
        run(filename, start, trigger_drop, delay, sense_mode)
    else:
        run('TFB006_H5_3.915', 3.9, 26.67, 0.1, '2W')
